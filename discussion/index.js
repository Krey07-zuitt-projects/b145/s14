// display message in console
console.log("Hello from JS");

// This is a single-line comment

/* Multi-line comment
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
*/

console.log("end of comment");

console.log("example statement");

// DECLARING VARIABLES

let myVariable;

let clientName = "Juan Dela Cruz";
let contactNumber = "0999999999";


// PEEKING INSIDE A VARIABLE

let greetings;

console.log(clientName);
console.log(contactNumber);
console.log(greetings);
let pangalan = "John Doe";
console.log(pangalan)


// Array

let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];
console.log(bootcampSubjects);

let details = ["Keanu", "Reeves", 32, true];
console.log(details);


let cellphone = {
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'AX12002122',
	isHomeCredit: true,
	features: ["Calling", "Texting", "Ringing", "5G"],
	price: 8000
}

console.log(cellphone);

// Variables and Constants


// Variables use let function


let pet = "dog";
console.log("this is the initial value of var:" + pet);

pet = "cat"
console.log("this is the new value of the variable:" + pet);

// constants


const familyName = 'Dela Cruz';
console.log(familyName);

